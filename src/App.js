import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { Login, Orders, OrderDetail, Calendar } from "./screens";
import history from "./utils/history";

const App = () => {
  return (
    <Router history={history}>
      <div>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/pedidos" exact component={Orders} />
          <Route path="/pedidos/:id" exact component={OrderDetail} />
          <Route path="/calendario" exact component={Calendar} />
          {/*<Route path="/login/new" exact component={StreamCreate} />
            <Route path="/streams/edit/:id" exact component={StreamEdit} />
            <Route path="/streams/delete/:id" exact component={StreamDelete} />
            <Route path="/streams/:id" exact component={StreamShow} />*/}
        </Switch>
      </div>
    </Router>
  );
};

export default App;
