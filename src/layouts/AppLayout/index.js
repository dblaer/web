import React, { Fragment } from "react";
import { NavHeader, SectionTitle } from "../../components";
import "./styles.scss";

export const AppLayout = props => {
  return (
    <Fragment>
      <NavHeader />
      <div className="ui container content-wrapper">
        <SectionTitle {...props} />
        {props.children}
      </div>
    </Fragment>
  );
};
