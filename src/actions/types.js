export const ORDERS_FETCHING = "orders_fetching";
export const ORDERS_FETCH_SUCCESS = "orders_fetch_success";

export const ORDER_FETCHING = "order_fetching";
export const ORDER_FETCH_SUCCESS = "order_fetch_success";

export const ORDER_SAVING = "order_saving";
export const ORDER_SAVE_SUCCESS = "order_save_success";
export const ORDER_RESET = "order_reset";

export const ORDER_PRODUCT_SAVING = "order_product_saving";
export const ORDER_PRODUCT_SAVE_SUCCESS = "order_product_save_success";
export const ORDER_PRODUCT_RESET = "order_product_reset";
