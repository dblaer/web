import firebase from "../utils/firebase";
import history from "../utils/history";

import {
  ORDERS_FETCHING,
  ORDERS_FETCH_SUCCESS,
  ORDER_FETCHING,
  ORDER_FETCH_SUCCESS,
  ORDER_SAVING,
  ORDER_RESET,
  ORDER_SAVE_SUCCESS,
  ORDER_PRODUCT_SAVING,
  ORDER_PRODUCT_SAVE_SUCCESS,
  ORDER_PRODUCT_RESET
} from "./types";

export const fetchOrders = () => dispatch => {
  dispatch({ type: ORDERS_FETCHING });

  var ref = firebase.database().ref(`/orders`);
  ref.on(
    "value",
    snapshot => {
      dispatch({ type: ORDERS_FETCH_SUCCESS, payload: snapshot.val() });
    },
    error => {
      console.log(`Error: ${error}`);
    }
  );
};

export const fetchOrder = id => dispatch => {
  dispatch({ type: ORDER_FETCHING });

  var ref = firebase.database().ref(`/orders/${id}`);
  ref.on(
    "value",
    snapshot => {
      dispatch({ type: ORDER_FETCH_SUCCESS, payload: snapshot.val() });
    },
    error => {
      console.log(`Error: ${error}`);
    }
  );
};

export const updateOrder = (id, { delivery, status }) => dispatch => {
  dispatch({ type: ORDER_SAVING });

  var ref = firebase.database().ref(`/orders/${id}`);
  ref
    .update({
      delivery,
      status
    })
    .then(() => {
      dispatch({ type: ORDER_SAVE_SUCCESS });
    });
};

export const resetOrderForm = () => dispatch => {
  dispatch({ type: ORDER_RESET });
};

export const resetOrderProductForm = () => dispatch => {
  dispatch({ type: ORDER_PRODUCT_RESET });
};

export const updateOrderProduct = (id, { products }) => dispatch => {
  dispatch({ type: ORDER_PRODUCT_SAVING });

  var ref = firebase.database().ref(`/orders/${id}`);
  ref
    .update({
      products
    })
    .then(() => {
      dispatch({ type: ORDER_PRODUCT_SAVE_SUCCESS });
    });
};
