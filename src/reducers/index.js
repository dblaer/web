import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import ordersReducer from "./ordersReducer";
import orderReducer from "./orderReducer";
import orderFormReducer from "./orderFormReducer";
import productFormReducer from "./productFormReducer";

export default combineReducers({
  form: formReducer,
  orders: ordersReducer,
  order: orderReducer,
  orderForm: orderFormReducer,
  productForm: productFormReducer
});
