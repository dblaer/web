import { ORDER_SAVING, ORDER_SAVE_SUCCESS, ORDER_RESET } from "../actions/types";

const INITIAL_STATE = {
  loading: false,
  success: false
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ORDER_SAVING:
      return { loading: true, success: false };
    case ORDER_SAVE_SUCCESS:
      return { loading: false, success: true };
    case ORDER_RESET:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
}
