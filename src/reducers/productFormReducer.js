import { ORDER_PRODUCT_SAVING, ORDER_PRODUCT_SAVE_SUCCESS, ORDER_PRODUCT_RESET } from "../actions/types";

const INITIAL_STATE = {
  loading: false,
  success: false
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ORDER_PRODUCT_SAVING:
      return { loading: true, success: false };
    case ORDER_PRODUCT_SAVE_SUCCESS:
      return { loading: false, success: true };
    case ORDER_PRODUCT_RESET:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
}
