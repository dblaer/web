import { ORDER_FETCHING, ORDER_FETCH_SUCCESS } from "../actions/types";

const INITIAL_STATE = {
  loading: true,
  error: "",
  data: {}
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ORDER_FETCHING:
      return { ...state, loading: true };
    case ORDER_FETCH_SUCCESS:
      return { loading: false, error: "", data: action.payload || {} };
    default:
      return state;
  }
}
