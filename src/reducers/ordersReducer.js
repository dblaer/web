import { ORDERS_FETCHING, ORDERS_FETCH_SUCCESS } from "../actions/types";

const INITIAL_STATE = {
  loading: false,
  error: "",
  data: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ORDERS_FETCHING:
      return { ...state, loading: true };
    case ORDERS_FETCH_SUCCESS:
      return { loading: false, error: "", data: action.payload || [] };
    default:
      return state;
  }
}
