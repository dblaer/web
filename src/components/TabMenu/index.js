import React, { Component } from "react";
import { Menu } from "semantic-ui-react";

export default class TabMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: this.props.items[0]
    };
  }

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name });
    this.props.onTabMenuItemClick(name);
  };

  render() {
    const { activeItem } = this.state;
    const { items } = this.props;

    return (
      <div>
        <Menu pointing secondary>
          {items.map(item => {
            return <Menu.Item key={item} name={item} active={activeItem === item} onClick={this.handleItemClick} />;
          })}
        </Menu>
      </div>
    );
  }
}
