import React from "react";
import "./styles.scss";
import { Link } from "react-router-dom";
import logo from "../../assets/logo-without-barcode.png";

export const NavHeader = props => {
  return (
    <nav className="nav-wrapper">
      <div className="ui container nav-content">
        <img src={logo} alt="logo" />
        <ul>
          <li className="active">
            <Link to="/pedidos">PEDIDOS</Link>
          </li>
          <li>
            <Link to="/">SALIR</Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};
