import React from "react";
import "./styles.scss";
import logo from "../../assets/logo.png";

export const Sidebar = props => {
  return (
    <div className="sidebar">
      <div className="nav-logo">
        <img src={logo} alt="logo" />
      </div>

      <nav className="menu">
        <li>
          <a href="#">
            <i className="home icon"></i>Home
          </a>
        </li>
        <li>
          <a href="#">
            <i className="tasks icon"></i>Pedidos
          </a>
        </li>
      </nav>
    </div>
  );
};
