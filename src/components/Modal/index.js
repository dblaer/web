import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./styles.scss";

class Modal extends Component {
  render() {
    const { header, active, onDismiss, children } = this.props;
    return ReactDOM.createPortal(
      <div onClick={onDismiss} className={`ui dimmer modals visible ${active ? "active" : ""}`}>
        <div
          onClick={e => {
            e.stopPropagation();
          }}
          className="ui tiny modal visible active"
        >
          <div className="header">{header}</div>
          {children}
        </div>
      </div>,
      document.querySelector("#modal")
    );
  }
}

export default Modal;
