import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import "./styles.scss";

export const SectionTitle = ({ title, breadcrumbs }) => {
  return (
    <div className="section-title">
      <div className="ui tiny breadcrumb">
        {breadcrumbs.map((b, i) => {
          let lastIndex = breadcrumbs.length - 1 === i;
          if (lastIndex) {
            return <div className="active section">{b}</div>;
          } else {
            return (
              <Fragment>
                <Link to={b.route} className="section">
                  {b.text}
                </Link>
                <i className="right chevron icon divider"></i>
              </Fragment>
            );
          }
        })}
      </div>
      <h1 className="ui header">{title}</h1>
    </div>
  );
};
