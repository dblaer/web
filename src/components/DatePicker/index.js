import React from "react";
import { default as ReactDatePicker } from "react-datepicker";
import { registerLocale } from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import es from "date-fns/locale/es";
registerLocale("es", es);

// CSS Modules, react-datepicker-cssmodules.css
// import 'react-datepicker/dist/react-datepicker-cssmodules.css';

class DatePicker extends React.Component {
  state = {
    startDate: this.props.startDate ? this.props.startDate : null
  };

  handleChange = date => {
    this.setState({
      startDate: date
    });
  };

  render() {
    return (
      <ReactDatePicker
        {...this.props}
        selected={this.state.startDate}
        value={this.state.startDate}
        onChange={this.handleChange}
        locale="es"
      />
    );
  }
}

export default DatePicker;
