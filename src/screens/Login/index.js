import React, { Component } from "react";
import logo from "../../assets/logo.png";
import history from "../../utils/history";
import "./styles.scss";

class Login extends Component {
  handleSubmit = values => {
    history.push("/pedidos");
  };

  render() {
    return (
      <div className="page-login">
        <div className="ui centered grid container">
          <div className="seven wide column">
            <div className="ui segment very padded center aligned form-container">
              <div className="content">
                <img className="ui centered image logo" src={logo} alt="logo" />
                <form className="ui form" onSubmit={this.handleSubmit} method="POST">
                  <div className="ui left icon input field fluid">
                    <input type="text" placeholder="Usuario" />
                    <i className="user icon"></i>
                  </div>
                  <div className="ui left icon input field fluid">
                    <input type="password" placeholder="Contraseña" />
                    <i className="lock icon"></i>
                  </div>
                  <div className="ui centered">
                    <button className="ui blue button" type="submit">
                      Login
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
