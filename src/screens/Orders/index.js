import React, { Component } from "react";
import { AppLayout } from "../../layouts";
import { TabMenu } from "../../components";
import history from "../../utils/history";
import * as actions from "../../actions";
import { connect } from "react-redux";
import {
  ORDER_STATUS_IN_PROCESS,
  ORDER_STATUS_CONFIRMED,
  ORDER_STATUS_FINISHED,
  ORDER_STATUS_CANCELLED
} from "../../utils/constants";
import _ from "lodash";
import "./styles.scss";

class Orders extends Component {
  state = {
    selectedStatus: ORDER_STATUS_IN_PROCESS.name
  };

  componentDidMount() {
    this.props.fetchOrders();
  }

  _onTabMenuItemClick = name => {
    switch (name) {
      case "En Proceso":
        this.setState({ selectedStatus: ORDER_STATUS_IN_PROCESS.name });
        break;
      case "Confirmados":
        this.setState({ selectedStatus: ORDER_STATUS_CONFIRMED.name });
        break;
      case "Finalizados":
        this.setState({ selectedStatus: ORDER_STATUS_FINISHED.name });
        break;
      case "Cancelados":
        this.setState({ selectedStatus: ORDER_STATUS_CANCELLED.name });
        break;
      default:
        break;
    }
  };

  _onOrderClick = order => {
    history.push(`/pedidos/${order.uid}`);
  };

  _renderOrderRow = (order, i) => {
    return (
      <tr key={order.uid} className="order-row" onClick={() => this._onOrderClick(order)}>
        <td className="center aligned">{i + 1}</td>
        <td className="center aligned">{order.products.length}</td>
        <td className="center aligned">${order.amount}</td>
        <td className="center aligned">{order.address.city}</td>
        <td className="center aligned">{order.date}</td>
        <td className="center aligned">{order.status.name}</td>
      </tr>
    );
  };

  _renderOrders = orders => {
    return (
      <table className="ui selectable small single line table">
        <thead>
          <tr>
            <th className="center aligned">#</th>
            <th className="center aligned">Productos</th>
            <th className="center aligned">Monto</th>
            <th className="center aligned">Localidad</th>
            <th className="center aligned">Fecha</th>
            <th className="center aligned">Estado</th>
          </tr>
        </thead>
        <tbody>{_.map(orders, (order, i) => this._renderOrderRow(order, i))}</tbody>
      </table>
    );
  };

  _renderEmptyList = () => {
    return (
      <div class="ui visible message">
        <p>No se encontraron resultados</p>
      </div>
    );
  };

  _renderLoading = () => {
    return (
      <div class="ui segment loader-container">
        <div class="ui active inverted dimmer">
          <div class="ui large text loader">Loading</div>
        </div>
        <p></p>
        <p></p>
        <p></p>
      </div>
    );
  };

  render() {
    const { selectedStatus } = this.state;
    const { ordersList, loading } = this.props;
    const filteredList = _.filter(ordersList, function(order) {
      return order.status.name.toLowerCase() === selectedStatus.toLowerCase();
    });

    return (
      <AppLayout title="Pedidos" breadcrumbs={["Pedidos"]}>
        <TabMenu
          items={["En Proceso", "Confirmados", "Finalizados", "Cancelados"]}
          onTabMenuItemClick={this._onTabMenuItemClick}
        />

        {loading
          ? this._renderLoading()
          : _.isEmpty(filteredList)
          ? this._renderEmptyList()
          : this._renderOrders(filteredList)}
      </AppLayout>
    );
  }
}

const mapStateToProps = ({ orders }) => {
  const { loading, data } = orders;

  const ordersList = _.map(data, (val, uid) => {
    return { ...val, uid };
  }).reverse();

  return { loading, ordersList };
};

export default connect(
  mapStateToProps,
  actions
)(Orders);
