import React, { Component, Fragment } from "react";
import { Field, reduxForm } from "redux-form";
import { Modal } from "../../../../components";
class ProductEditModal extends Component {
  renderError = ({ error, touched }) => {
    if (touched && error) {
      return <div className="ui mini error message">{error}</div>;
    }
  };

  renderInput = ({ input, label, meta, readonly }) => {
    return (
      <div className="field">
        <label>{label}</label>
        <input {...input} placeholder={label} readOnly={readonly == "true"} />
        {this.renderError(meta)}
      </div>
    );
  };

  renderPriceInput = ({ input, label, meta, readonly }) => {
    return (
      <div className="field">
        <label>{label}</label>
        <div class="ui labeled input">
          <div class="ui label">$</div>
          <input {...input} placeholder={label} readOnly={readonly == "true"} />
        </div>
        {this.renderError(meta)}
      </div>
    );
  };

  onSubmit = formValues => {
    this.props.onSubmit(formValues);
  };

  renderSuccessMessage = () => {
    const { success } = this.props;
    if (success) {
      return (
        <div class="ui success message">
          <div class="header">Cambios realizados</div>
          <p>El pedido ha sido actualizado exitosamente</p>
        </div>
      );
    }
    return null;
  };

  render() {
    const { active, onDismiss, loading, success } = this.props;
    return (
      <Modal active={active} header="Editar Producto" onDismiss={onDismiss}>
        <div className="content">
          <form id="theform" class="ui form" onSubmit={this.props.handleSubmit(this.onSubmit)}>
            <Field name="code" readonly="true" component={this.renderInput} label="Codigo" />
            <Field name="name" readonly="true" component={this.renderInput} label="Nombre" />
            <Field name="unitPrice" readonly="true" component={this.renderPriceInput} label="Precio Unitario" />
            <Field name="quantity" readonly="false" component={this.renderInput} label="Cantidad" />
          </form>
          {this.renderSuccessMessage()}
        </div>
        <div className="actions">
          <button className={`ui button blue ${loading ? "loading" : ""}`} form="theform" type="submit">
            Guardar
          </button>
          <div className="ui button" onClick={this.props.onDismiss}>
            Cancelar
          </div>
        </div>
      </Modal>
    );
  }
}

export default reduxForm({
  form: "productEditForm",
  enableReinitialize: true
})(ProductEditModal);
