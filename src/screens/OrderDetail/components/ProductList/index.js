import React, { Fragment } from "react";
import ProductItem from "../ProductItem/index";
import _ from "lodash";
import "./styles.scss";

export const ProductList = props => {
  const { products, enableProductEdit, onProductEditClick } = props;
  return (
    <div className="products">
      <h3 className="ui dividing header">Productos</h3>
      <div className="ui divided items products-list">
        {_.map(products, (product, i) => (
          <ProductItem
            key={i}
            product={product}
            enableProductEdit={enableProductEdit}
            onProductEditClick={onProductEditClick}
          />
        ))}
      </div>
      <div class="ui divider"></div>
      <div className="total-amount">
        <span>Total</span>
        <span>
          $
          {_.sumBy(products, function(p) {
            return p.unitPrice * p.quantity;
          })}
        </span>
      </div>
    </div>
  );
};
