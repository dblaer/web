import React, { Component } from "react";
import { DatePicker } from "../../../../components";
import { Field, reduxForm } from "redux-form";
import { ORDER_STATUS, ORDER_STATUS_CONFIRMED } from "../../../../utils/constants";
import moment from "moment";
import _ from "lodash";
import "./styles.scss";

class OrderForm extends Component {
  renderError = ({ error, touched }) => {
    if (touched && error) {
      return <div className="ui mini error message">{error}</div>;
    }
  };

  renderInput = ({ input, label, meta, readonly }) => {
    return (
      <div className="field">
        <label>{label}</label>
        <input {...input} placeholder={label} readOnly={readonly == "true"} />
        {this.renderError(meta)}
      </div>
    );
  };

  renderDatepicker = ({ input, label, meta, readonly, timeOnly }) => {
    return (
      <div className="field">
        <label>{label}</label>
        {timeOnly ? (
          <DatePicker
            {...input}
            placeholderText={label}
            name={input.name}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={30}
            startDate={input.value ? moment(input.value, "HH:mm").toDate() : null}
            timeCaption="Horario"
            dateFormat="HH:mm"
          />
        ) : (
          <DatePicker
            {...input}
            startDate={input.value ? moment(input.value, "DD/MM/YYYY").toDate() : null}
            dateFormat="dd/MM/yyyy"
            placeholderText={label}
            name={input.name}
          />
        )}
        {this.renderError(meta)}
      </div>
    );
  };

  renderSelect = ({ input, label, meta, readonly, children }) => {
    return (
      <div class="field">
        <label>{label}</label>
        <select {...input} class="ui fluid dropdown" value={input.value} name={input.name}>
          {children}
        </select>
      </div>
    );
  };

  onSubmit = formValues => {
    var newStatus = _.find(ORDER_STATUS, function(o) {
      return o.code == formValues.status.code;
    });
    this.props.onSubmit({ ...formValues, status: newStatus });
  };

  renderSuccessMessage = () => {
    const { success } = this.props;
    if (success) {
      return (
        <div class="ui success message">
          <div class="header">Cambios realizados</div>
          <p>El pedido ha sido actualizado exitosamente</p>
        </div>
      );
    }
    return null;
  };

  render() {
    return (
      <div>
        <form class="ui tiny form" autoComplete="off" onSubmit={this.props.handleSubmit(this.onSubmit)}>
          <h4 class="ui dividing header">General</h4>
          <div class="field">
            <div class="two fields">
              <Field name="date" readonly="true" component={this.renderInput} label="Fecha de Compra" />
              <Field name="status[code]" readonly="false" component={this.renderSelect} label="Estado">
                {_.map(ORDER_STATUS, status => (
                  <option key={status.code} value={status.code}>
                    {status.name}
                  </option>
                ))}
              </Field>
            </div>
          </div>
          <h4 class="ui dividing header">Entrega</h4>
          <div class="field">
            <div class="two fields">
              <Field name="address[city]" readonly="true" component={this.renderInput} label="Ciudad" />
              <Field name="address[street]" readonly="true" component={this.renderInput} label="Calle" />
            </div>
          </div>
          <div className="field">
            <div className="four fields">
              <Field name="address[number]" readonly="true" component={this.renderInput} label="Número" />
              <Field name="address[door]" readonly="true" component={this.renderInput} label="Piso/Departamento" />
              <Field name="address[zipCode]" readonly="true" component={this.renderInput} label="Código Postal" />
              <Field name="address[betweenStreets]" readonly="true" component={this.renderInput} label="Entre Calles" />
            </div>
          </div>
          <div className="field">
            <div className="three fields">
              <Field name="delivery[date]" component={this.renderDatepicker} label="Fecha de Entrega" />
              <Field name="delivery[fromHour]" component={this.renderDatepicker} label="Desde" timeOnly />
              <Field name="delivery[toHour]" component={this.renderDatepicker} label="Hasta" timeOnly />
            </div>
          </div>
          <h4 class="ui dividing header">Tarjeta de Credito</h4>
          <div class="field">
            <div class="two fields">
              <Field name="creditCard[brand]" readonly="true" component={this.renderInput} label="Marca" />
              <Field name="creditCard[number]" readonly="true" component={this.renderInput} label="Número" />
            </div>
            <div class="field">
              <div class="two fields">
                <Field name="creditCard[cardHolder]" readonly="true" component={this.renderInput} label="Titular" />
                <Field name="creditCard[dueDate]" readonly="true" component={this.renderInput} label="Vencimiento" />
              </div>
            </div>
          </div>
          <h4 class="ui dividing header">Contacto</h4>
          <div class="field">
            <div class="two fields">
              <Field name="contact[areaCode]" readonly="true" component={this.renderInput} label="Código de Área" />
              <Field name="contact[number]" readonly="true" component={this.renderInput} label="Número" />
            </div>
          </div>
          <div className="submit-container">
            <button className={`ui button blue ${this.props.loading ? "loading" : ""}`} type="submit">
              Guardar
            </button>
          </div>
        </form>
        {this.renderSuccessMessage()}
      </div>
    );
  }
}

const validate = formValues => {
  const errors = {};

  /*if (formValues.status.code === ORDER_STATUS_CONFIRMED.code) {
    errors.delivery.date = "Debe confirmar la fecha de entrega";
  }*/
  return errors;
};

export default reduxForm({
  form: "orderForm",
  validate,
  enableReinitialize: true
})(OrderForm);
