import React, { Component } from "react";
import "./styles.scss";

class ProductItem extends Component {
  render() {
    const { product, enableProductEdit, onProductEditClick } = this.props;
    const editableClass = enableProductEdit ? "editable" : "";
    return (
      <div className="item">
        <div className="product">
          <div className="info">
            <div className="details">
              <span className="title">{product.name}</span>
              <span className="quantity">{product.quantity} unidades</span>
            </div>
            <div className="price">${product.quantity * product.unitPrice}</div>
          </div>
          <div className={`actions ${editableClass}`}>
            <button onClick={() => onProductEditClick(product)} class="ui inverted blue button">
              EDITAR
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductItem;
