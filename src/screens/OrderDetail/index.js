import React, { Component } from "react";
import { AppLayout } from "../../layouts";
import { ProductList, OrderForm, ProductEditModal } from "./components";
import * as actions from "../../actions";
import { connect } from "react-redux";
import _ from "lodash";
import { ORDER_STATUS_IN_PROCESS } from "../../utils/constants";

class OrderDetail extends Component {
  state = {
    modalActive: false,
    selectedProduct: null
  };

  componentDidMount() {
    this.props.resetOrderForm();
    this.props.fetchOrder(this.props.match.params.id);
  }

  _renderLoading = () => {
    return (
      <div className="loader-container">
        <div class="ui active inverted dimmer ">
          <div class="ui large text loader">Loading</div>
        </div>
        <p></p>
        <p></p>
        <p></p>
      </div>
    );
  };

  onOrderFormSubmit = formValues => {
    this.props.updateOrder(this.props.match.params.id, formValues);
  };

  onProductFormSubmit = formValues => {
    const { order } = this.props;
    _.find(order.products, { code: formValues.code }).quantity = formValues.quantity;
    this.props.updateOrderProduct(this.props.match.params.id, order);
  };

  onModalOpen = product => {
    this.props.resetOrderProductForm();
    this.setState({ modalActive: true, selectedProduct: product });
  };

  onModalDismiss = () => {
    this.setState({ modalActive: false, selectedProduct: null });
  };

  render() {
    const { loading, order, orderFormLoading, orderFormSuccess, productFormLoading, productFormSuccess } = this.props;
    return (
      <AppLayout title="Detalle del Pedido" breadcrumbs={[{ text: "Pedidos", route: "/pedidos" }, "Detalle"]}>
        <div class="ui grid">
          <div class="ten wide column">
            <div className="ui padded segment">
              {loading ? (
                this._renderLoading()
              ) : (
                <OrderForm
                  initialValues={order}
                  loading={orderFormLoading}
                  success={orderFormSuccess}
                  onSubmit={this.onOrderFormSubmit}
                />
              )}
            </div>
          </div>
          <div class="six wide column">
            <div className="ui padded segment">
              {loading ? (
                this._renderLoading()
              ) : (
                <ProductList
                  products={order.products}
                  enableProductEdit={order.status.code == ORDER_STATUS_IN_PROCESS.code}
                  onProductEditClick={this.onModalOpen}
                />
              )}
            </div>
          </div>
        </div>
        <ProductEditModal
          initialValues={this.state.selectedProduct}
          onDismiss={this.onModalDismiss}
          onSubmit={this.onProductFormSubmit}
          active={this.state.modalActive}
          loading={productFormLoading}
          success={productFormSuccess}
        />
      </AppLayout>
    );
  }
}

const mapStateToProps = ({ order, orderForm, productForm }) => {
  const { loading, data } = order;
  return {
    loading,
    order: data,
    orderFormLoading: orderForm.loading,
    orderFormSuccess: orderForm.success,
    productFormLoading: productForm.loading,
    productFormSuccess: productForm.success
  };
};

export default connect(
  mapStateToProps,
  actions
)(OrderDetail);
