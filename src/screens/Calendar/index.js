import React, { Component } from "react";
import { AppLayout } from "../../layouts";
import history from "../../utils/history";
import * as actions from "../../actions";
import { connect } from "react-redux";
import _ from "lodash";
import { ORDER_STATUS_CONFIRMED } from "../../utils/constants";

class Calendar extends Component {
  componentDidMount() {
    this.props.fetchOrders();
  }

  _renderOrders = orders => {};

  render() {
    const { loading, ordersList } = this.props;
    const grouped = _.chain(ordersList)
      .filter(function(o) {
        return o.status.code == ORDER_STATUS_CONFIRMED.code;
      })
      .groupBy(function(order) {
        return order.delivery.date;
      })
      .value();

    return <AppLayout title="Calendario" breadcrumbs={["Calendario"]}></AppLayout>;
  }
}

const mapStateToProps = ({ orders }) => {
  const { loading, data } = orders;

  const ordersList = _.map(data, (val, uid) => {
    return { ...val, uid };
  }).reverse();

  return { loading, ordersList };
};

export default connect(
  mapStateToProps,
  actions
)(Calendar);
