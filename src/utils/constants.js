export const ORDER_STATUS_IN_PROCESS = {
  code: 0,
  name: "EN PROCESO",
  description: "Su pedido todavia no ha sido preparado"
};
export const ORDER_STATUS_CONFIRMED = {
  code: 1,
  name: "CONFIRMADO",
  description: "Su pedido ha sido preparado y lo recibirá en la fecha acordada"
};
export const ORDER_STATUS_FINISHED = {
  code: 2,
  name: "FINALIZADO",
  description: "Su pedido ha sido entregado exitosamente"
};
export const ORDER_STATUS_CANCELLED = {
  code: 3,
  name: "CANCELADO",
  description: "Su pedido ha sido cancelado"
};

export const ORDER_STATUS = [
  ORDER_STATUS_IN_PROCESS,
  ORDER_STATUS_CONFIRMED,
  ORDER_STATUS_FINISHED,
  ORDER_STATUS_CANCELLED
];
