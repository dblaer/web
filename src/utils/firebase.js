import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyCLe2WXAfx6Ic4iKQo3C1E_-wtPdqQcYcg",
  authDomain: "smarket-dev-87ab8.firebaseapp.com",
  databaseURL: "https://smarket-dev-87ab8.firebaseio.com",
  projectId: "smarket-dev-87ab8",
  storageBucket: "smarket-dev-87ab8.appspot.com",
  messagingSenderId: "1075870598774",
  appId: "1:1075870598774:web:97a96c9275271a01"
};
// Initialize Firebase
const fire = firebase.initializeApp(firebaseConfig);
export default fire;
